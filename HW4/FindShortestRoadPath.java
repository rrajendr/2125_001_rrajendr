import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
 * Dijkstra's Algorithm Implementation
 * Solve the single shortest paths problem
 */

public class FindShortestRoadPath {
	
	public static ArrayList<Integer> nodesValues = new ArrayList<>();
	
    @SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException{
    	
    	/*
    	 * Check if given input is correct
    	 */
        if(args.length != 3){
        	System.out.println("\nBad User. Use guidelines \n");
            System.out.println("java FindShortestRoadPath datafile sourceID destinationID");
            System.out.println("java FindShortestRoadPath ny.gr 1 2188");
            System.exit(1);
        }
        
    	//Declare output file
        File f = new File("Output.txt");
        
        //Declare writer
        PrintWriter writer = new PrintWriter(f);
        
        //Take file as input
        File inputFile = new File(args[0]);
        
        //Source Node
        int sourceID = Integer.parseInt(args[1]);
        
        //Destination Node
        int targetID = Integer.parseInt(args[2]);
        
        //Scanner object to read the input file
		Scanner readFile = new Scanner(inputFile);
        Scanner readFileNode = new Scanner(inputFile);
        
        //Initialise strings
        String line = "";
        String lineNode = "";
        
        int sizeOfNode = 0;
        
        //Calculate size of the node
        while(readFileNode.hasNextLine()){
            lineNode = readFileNode.nextLine();
            String[] tokensNode = lineNode.split(" ");
            if(tokensNode[0].equals("p")){
                sizeOfNode = Integer.parseInt(tokensNode[2]);
                break;
            }     
        }
          
        sizeOfNode++;
        
        //Declare new graph
        Graph graph = new Graph(sizeOfNode);
        
        //Add vertices
        for (int i = 1; i <= sizeOfNode; i++){
            graph.addVertex(i);
        }
        
        //Read and connect the graph
        while(readFile.hasNextLine()){
            line = readFile.nextLine();
            
            String[] tokens = line.split(" ");
            
            if(tokens[0].equals("a")){
                int startNode = Integer.parseInt(tokens[1]);
                int endNode = Integer.parseInt(tokens[2]);
                int weight = Integer.parseInt(tokens[3]); 
            
                graph.addEdge(startNode, endNode, weight);
            }     
        }    

        //Answer 1 and 2
        String unoAnswer = graph.shortestPath(sourceID, targetID);
        ArrayList<Integer> dosAnswer = graph.printPath(sourceID, targetID);

        //Write Output to file
        writer.println(unoAnswer + "\n");
        writer.println("Path: ");        
       
        for (int i =0; i< dosAnswer.size(); i++){
            writer.println(dosAnswer.get(i));
        }
        
        writer.close();			//Thou shall always close the writer 
        
        outputCoordinates();
    }

    private static void outputCoordinates() throws FileNotFoundException {
		File coordinatesFile = new File("Coordinates.txt");
		
		PrintWriter writer = new PrintWriter(coordinatesFile);
		
		File inputFile = new File("ny.co");
		
		Scanner scanner = new Scanner(inputFile);
		
		String coordinates = "";
		
		ArrayList<Point> outputCoordinates = new ArrayList<Point>();
		
		int xAxis = 0;
		int yAxis = 0;
		
		//Read and connect the graph
        while(scanner.hasNextLine()){
        	coordinates = scanner.nextLine();
            
            String[] tokens = coordinates.split(" ");
            
            if(tokens[0].equals("v")){
            	for (int i =0; i< nodesValues.size(); i++){
                    int node = nodesValues.get(i);
                    if(node == Integer.parseInt(tokens[1])) {
                    	xAxis = Integer.parseInt(tokens[2]);
                    	yAxis = Integer.parseInt(tokens[3]);
                    	outputCoordinates.add(new Point(xAxis, yAxis));
                    }
                }
            }     
        }
        
        writer.println("[");
        for(Point point: outputCoordinates) {
        	writer.println("{lat: "+point.x+", lng: "+point.y+"},");
        }
        writer.println("]");
        
        writer.close();			//Thou shall always close the writer
	}
    
	/*
     * Implementing graph based on the vertices and weight provided
     */
    public static class Graph{
    	
        Vertex[] vertexes;
        int maxSize;
        int size;
        
        /*
         * Constructor Graph
         */
        public Graph(int maxSize) {
            this.maxSize = maxSize;
            vertexes = new Vertex[maxSize];
        }
 
        private enum State{
            NEW, 					//Not Visited
            IN_QUEUE, 				//Currently Visiting
            OLD						//Already Visited
        }

        /*
         * Vertex Object
         */
        public class Vertex {
        	
            int cost;
            int name;
            int degree;
            
            Buddy adjacent;
                        
            State state;
            Vertex prev;
            
            //Constructor Vertex
            public Vertex(int name) {
                this.name = name;
                cost = Integer.MAX_VALUE;
                state = State.NEW;
                prev = null;
            }
            
            /**
            * Return 1, 0 or -1 if greater equal or less than
            *
            *@param v Vertex to Compare
            *@return int (1, 0 ,-1) if greater, equal or less than
            */
            public int compareTo(Vertex v) {
                if (this.cost == v.cost) {
                    return 0;						//If equal
                }
                if (this.cost < v.cost) {
                    return -1;						//if cost is smaller
                }
                return 1;							//if greater
            }
        }

        /*
         * Neighbor of Vertex
         */
        private class Buddy {
            int index;
            Buddy next;
            int weight;
            Vertex prev;
            
            /*
             * Constructor Neighbor
             */
            Buddy(int index, int weight, Buddy next) {
                this.index = index;
                this.next = next;
                this.weight = weight;
                prev = null;
            }
        }

        //add Vertexes 
        public void addVertex(int name) {
            vertexes[size++] = new Vertex(name);
        }

        //add Edge
        public void addEdge(int source, int destination, int weight) {
            int sourceIndex = source;
            int destinationIndex = destination;
            vertexes[sourceIndex].adjacent = new Buddy(destinationIndex, weight, vertexes[sourceIndex].adjacent);
            vertexes[destinationIndex].degree++;
        }

        /*
         * Calculate shortest path
         */
        public String shortestPath(int source, int destination){
            Dijkstra(vertexes[source]);
            {
                String shortestPath = "Shortest Path from "+ source + " to " + vertexes[destination-1].name + " is : \nWeight = " + vertexes[destination].cost;
                return shortestPath;
            }
        }

        

        /*
        *Queue Object using heaps to perform Dijkstra
        *Minimum Priority Queue
        */
        private static class Queue {
        	
            private Vertex[] queue;
            private int maxSize;
            private int size;
            
            //Constructor Queue
            private Queue(int maxSize) {
                this.maxSize = maxSize;
                queue = new Vertex[maxSize];
            }

            //
            /**
    * Return if Queue is Empty
    * @return size
    */
            private boolean isEmpty() {
                return size == 0;
            }

            //Adds Vertexes
            /**
    * Print shortest path from Source to Target 
    * @param destination_not_that_one The distance between the vextexes
    * @return Shortest Path
    */
            private void add(Vertex u) {
                queue[size++] = u;
                moveUP(size - 1);
            }

            //
            /**
    * Removes vertexes
    * @param vertex Current vertex
    * @return Removed vertex
    */
            private Vertex remove() {
                Vertex vertex = queue[0];
                swap(0, size - 1);
                queue[size - 1] = null;
                size--;
                moveDown(0);
                return vertex;
            }

            /**
    * helper method to restore the heap condition for increased priority
    */
            private void moveUP(Vertex u) {
                for (int i = 0; i < maxSize; i++) {
                    if (u == queue[i]) {
                        moveUP(i);
                        break;
                    }
                }
            }
   /**
    * Restore heap condition
    * @param currIndex Current Index
    * @param leftChildIndex Index of the Left Child
    * @param rightChildIndex Index of the right Child
    * @param curr Current
    * @param childIndex Index of child
    * @param childItem The child Item
    */
            private void moveUP(int position) {
                int currIndex = position;
                Vertex curr = queue[currIndex];
                
                int parentIndex = (currIndex - 1) / 2;
                Vertex parentItem = queue[parentIndex];
                
                while (curr.compareTo(parentItem) == -1) {
                    swap(currIndex, parentIndex);
                    currIndex = parentIndex;
                    if (currIndex == 0) {
                        break;
                    }
                    
                    curr = queue[currIndex];
                    parentIndex = (currIndex - 1) / 2;
                    parentItem = queue[parentIndex];
                }
            }
            
            //Method to restore the heap condition (may have some inspiration from another code far far away

            /**
             * Restore heap condition
             * @param currIndex Current Index
             * @param leftChildIndex Index of the Left Child
             * @param rightChildIndex Index of the right Child
             * @param curr Current
             * @param childIndex Index of child
             * @param childItem The child Item
             */
                     private void moveDown(int postion) {
                         if (size == 1) {
                             return;
                         }

                         int currIndex = postion;
                         Vertex curr = queue[currIndex];
                         
                         int leftChildIndex = currIndex * 2 + 1;
                         int rightChildIndex = currIndex * 2 + 2;
                         int childIndex;

                         if (queue[leftChildIndex] == null) {
                             return;
                         }
                         
                         if (queue[rightChildIndex] == null) {
                             childIndex = leftChildIndex;
                         }else if (queue[rightChildIndex].compareTo(queue[leftChildIndex]) == -1){
                             childIndex = rightChildIndex;
                         }else{
                             childIndex = leftChildIndex;
                         }

                         Vertex childItem = queue[childIndex];

                         while (curr.compareTo(childItem) == 1) {
                             swap(currIndex, childIndex);
                             currIndex = childIndex;
                             curr = queue[currIndex];
                             leftChildIndex = currIndex * 2 + 1;
                             rightChildIndex = currIndex * 2 + 2;
                             if (queue[leftChildIndex] == null) {
                                 return;
                             }
                             if (queue[rightChildIndex] == null) {
                                 childIndex = leftChildIndex;
                             } else if (queue[rightChildIndex].compareTo(queue[leftChildIndex]) == -1) {
                                 childIndex = rightChildIndex;
                             } else {
                                 childIndex = leftChildIndex;
                             }
                         }
                     }
            
            /**
    * Swap two positions
    * @param neighbour Neighbor of vertex
    */
            private void swap(int position1, int position2) {
                Vertex neighbour = queue[position1];
                queue[position1] = queue[position2];
                queue[position2] = neighbour;
            }
        }
        
        /**
        *Dijkstra method performs Dijkstra's Algorithm
        *Add the source vertex to queue, update its state and cost
        *Remove from queue, update state
        *Get its connected nodes and calculate their costs and update the states
        *keep repeating and updating for every node
        *
        *NOTE: that queue is used as it is faster. My earlier implementations simply
        *using arrays took approx 9 mins to calculate the output
        *
        *@param source  vertex to start dijkstra algorith from 
        */
        private void Dijkstra(Vertex source) {
            Queue queue = new Queue(maxSize);
            queue.add(source);
            source.state = State.IN_QUEUE;
            source.cost = 0;
            
            while (!queue.isEmpty()) {
                Vertex removedOne = queue.remove();
                removedOne.state = State.OLD;
                /*
                 * Get the neighbouring nodes and travese
                 * Update their states and costs
                 * Keep track of the previous node
                 * Will be used to trace the shortest path back
                 */
                
                Buddy neighbour = removedOne.adjacent;
                
                while (neighbour != null) {
                    if (vertexes[neighbour.index].state == State.NEW) {
                        queue.add(vertexes[neighbour.index]);
                        vertexes[neighbour.index].state = State.IN_QUEUE;
                    }
                    if (vertexes[neighbour.index].cost > removedOne.cost + neighbour.weight) {
                        vertexes[neighbour.index].cost = removedOne.cost + neighbour.weight;                     
                        vertexes[neighbour.index].prev = removedOne;
                        queue.moveUP(vertexes[neighbour.index]);
                    }
                    neighbour = neighbour.next;
                }
            }
        }

        /**
    * Print shortest path from Source to Target 
    * @param destination_not_that_one The distance between the vextexes
    * @return Shortest Path
    */
        public ArrayList<Integer> printPath(int source, int destination){
        	
            Vertex destination_not_that_one = vertexes[destination];				//won't let me name it destination
            
            ArrayList<Integer> output2 = new ArrayList<>();
            
            while((destination_not_that_one.name-1) != source){           
                output2.add(destination_not_that_one.name - 1);
                destination_not_that_one= destination_not_that_one.prev;
            }
            
            output2.add(source);
            
            //Reverse to get proper list
            Collections.reverse(output2);
            
            nodesValues = output2;
            
            return output2;
        }
    }
}