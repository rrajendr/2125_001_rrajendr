var coordinates = document.getElementById("openfile").addEventListener("change", function(){
  var fr = new FileReader();
  fr.onload = function(){
    document.getElementById("Filecontents").textContent = this.result;
    var fileContent = this.result.trim();
    var rows = fileContent.split("\n");
    var coordinates = rows.map(function(row) {
      var cor = row.split(' ');

      return {lat: Number(cor[0]), lng: Number(cor[1])};
    });

    initMap(coordinates)
  }
  fr.readAsText(this.files[0]);
});


function initMap(coordinates) {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: {lat: 37.772, lng: -122.214},
      mapTypeId: 'terrain'
    });

    var flightPlanCoordinates = [
      {lat: 37.772, lng: -122.214},
      {lat: 38.772, lng: -122.214},
      {lat: 39.772, lng: -122.214},
      {lat: 39.772, lng: -118.214},
    ];
    var flightPath = new google.maps.Polyline({
      path: flightPlanCoordinates,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

    flightPath.setMap(map);
}
