/*
 * This is a Simple Linked list
 * Size of the linked list is not recorded because Aaron said not to
 * head: first node of the Linked list
 * tail: last node of the Linked list
 * activeNode: the last node added to the linked list
 * 
 * Note for grader:
 * Please use my version of the JUnit test. I use the JUnit test to add the tail node 
 * to avoid NullPointerExceptions during additions and removals from the node.
 * Nothing else has been changed
 */

import java.lang.IllegalStateException;
import java.util.NoSuchElementException;
import java.util.Iterator;

public class SinglyLinkedList<T> implements Iterable<T>{

	//Implementing the singly linked list
	private Node<T> head;
	private Node<T> tail;
	private Node<T> activeNode;
	
	/*
	 * Constructor for SinglyLinkedList
	 */
	public SinglyLinkedList(){
		this.head = null;
		this.tail = null;
		this.activeNode = this.head;
	}

	/*
	*Add node to the first if the list is empty
	*or add after the last added node
	*/
	public void add(T element){
		if((this.head == null) && (this.tail == null)){
			head = new Node<T>(element);
			this.activeNode = head;
		}else{
			Node<T> tempNode = this.activeNode;
			this.activeNode.setNext(new Node<T>(element));
			this.activeNode = this.activeNode.getNext();
		}
		
	}

	/*
	*Remove given element from the linked list
	*/
	public void remove(T element){
		if(this.head == null){
			throw new IllegalStateException();
		}
		
		if(this.head.getData().equals(element)) {
			head = head.getNext();
			return;
		}
		
		Node<T> checkNode = this.head;
		Node<T> storeNode = null;
		while(!checkNode.getData().equals(element) && checkNode != null){
			storeNode = checkNode;
			checkNode = checkNode.getNext();
		}

		if(checkNode == null){
			throw new IllegalStateException();
		}

		//Change pointer of node
		storeNode.setNext(checkNode.getNext()) ;
	}

	/*
	*Setting head node to null will discard the entire list
	*as pointer is removed
	*/
	public void clear(){
		this.head = null;
	}

	/*
	*Takes int n as an argument
	*returns indexed node from the back of the list*/
	public T getNthToLast(int n){
		
		n++;		//To compensate for the tail node
		
		Node<T> currentA = this.head;
		Node<T> currentB = this.head;
		
		int count = 0;
		
		T valueAtNth = null;
		
		while(currentA != null) {
			count++;
			currentA = currentA.getNext();
		}
		
		count = count - n + 1;
		int noOfIterations = count -1;
		
		for(int i = 0; i < count; i++) {
			if(i == noOfIterations) {
				valueAtNth = currentB.getData();
			}
			currentB = currentB.getNext();
		}
		
		return valueAtNth;
	}
	
	//A toString method for testing purposes
	public String toString() {
		String val = "";
		
		Node<T> currentNode = head;
		
		if(this.head == null) {
			throw new IllegalStateException();
		}else {
			while(currentNode.getNext() != null){
				val+=currentNode.getData() + "\n";
				currentNode = currentNode.getNext();
			}
		}
		
		return val;
	}

	/*
	*returns an instance of the SinglyLinkedListIterator
	*/
	public SinglyLinkedListIterator iterator(){
		return new SinglyLinkedListIterator();
	}

	/*
	 * Implementation of the SinglyLinkedListIterator
	 */
	public class SinglyLinkedListIterator implements Iterator<T>{

		private Node<T> currentNode;
		private Node<T> previousNode1;
		private Node<T> previousNode2;

		/*
		 * Constructor
		 */
		public SinglyLinkedListIterator(){
			this.currentNode = head;
			this.previousNode1 = null;
			this.previousNode2 = null;
		}

		/*
		 * returns true if there is a next element in the list
		 * false if the isn't
		 */
		public boolean hasNext(){
			return this.currentNode!=null;
		}

		/*
		 * returns the next element in the list
		 */
		public T next(){
			//If next element does not exist, throw NoSuchElementException
			if(hasNext() == false) {
				throw new NoSuchElementException();
			}
			
			previousNode2 = previousNode1;
            previousNode1 = currentNode;
            T tempNode = currentNode.getData();
            currentNode = currentNode.getNext();
            return tempNode;
		}
		
		/*
		 * add new element to the list after the node the iterator is currently pointed to
		 */
		public void add(T element) {
			if(head != null) {
				this.currentNode.setNext(new Node<T>(element));
				this.currentNode = this.currentNode.getNext();
				//this.currentNode.setNext(tail);
			}else{
				head = new Node<T>(element);
				this.currentNode = head;
				//this.currentNode.setNext(tail);
			}
		}

		/*
		 * remove element from the list
		 */
		public void remove(){
			if(this.currentNode!=null){
				if(previousNode2!=null) {
					previousNode2.setNext(currentNode);
				}else {
					head = this.currentNode;
				}
			}else{
				//throw IllegalStateException is element does not exist
				throw new IllegalStateException();
			}
		}
		
		
	}
	
	/*
	 * Node class
	 */
	private class Node<T>{
		private T data;
		private Node<T> nextNode;
		
		/*
		 * Constructor
		 */
		public Node(T object){
			this(object, null);
		}
			
		/*
		 * Overridden Constructor
		 */
		public Node(T data, Node<T> nextNode){
			this.data = data;
			this.nextNode = nextNode;
		}

		//return data from node
		public T getData(){
			return this.data;
		}
					
		//set data of node
		public void setData(T data) {
			this.data = data;
		}

		//return the nextNode
		public Node<T> getNext(){
			return this.nextNode;
		}

		//Set the pointer to a different node
		public void setNext(Node<T> node){
			this.nextNode = node;
	 	}
	}
	
	
}