import java.util.Random;
import java.util.concurrent.TimeUnit;

/*
 * @author: Rishav Rajendra
 * @Class: CSCI 2125
 * This class is an Implementation of AVLTree
 * An AVLTree is a balanced binary search tree 
 * Couldn't get the PrintAVLTree to get working. But everything except that works
 */

public class AVLTree<E extends Comparable<E>>{
	
	//Declare class variables
	AVLNode<E> root;
	private static final int allowedImbalance = 1;
	
	/*
	 * Create AVL tree
	 * Constructor for AVLTree
	 */
	public AVLTree() {
		this.root = null;
	}

	/*
	 * Return height of node
	 * -1 if null
	 */
	private int height(AVLNode<E> t) {
		return t == null ? -1 : t.height;
	}
	
	/*
	 * Insert a node into the AVLTree
	 * Calls a helper method insert(Element, root)
	 */
	public void insert(E x) {
		root = insert(x, root);
	}
	
	/*
	 * Remove a node from the AVLTree
	 * Calls a helper method remove(Element, root)
	 */
	public void remove(E x) {
		root = remove(x, root);
	}
	
	/*
	 * Check if element is in the AVLTree
	 * calls a helper method contains(Element, root)
	 */
	public boolean contains(E x) {
		return contains(x, root);
	}
	
	/*
	 * returns root of tree
	 */
	public AVLNode<E> getRoot(){
		return this.root;
	}
	
	//Helper method to insert AVLNode into AVLTree.
	private AVLNode<E> insert(E x, AVLNode<E> t){
		if(t == null) {
			return new AVLNode<E>(x);						//Tree empty. Add new Node
		}
		
		int compareResult = x.compareTo(t.element);			//Check if new node is greater or smaller than current node
		
		if(compareResult<0) {
			//if smaller
			t.left = insert(x, t.left);
		}else if(compareResult > 0) {
			//if greater
			t.right = insert(x, t.right);
		}else {
			/*
			 * Duplicate node
			 * do nothing
			 */
		}
		
		//Balance the tree. Then return
		return balance(t);
	}
	
	//Helper method to balance AVLTree. Called after every insertion or deletion
	private AVLNode<E> balance(AVLNode<E> t){
		if(t == null) {
			return t;												//tree empty
		}
		if(height(t.left) - height(t.right) > allowedImbalance) {
			//ensure single rotation
			if(height(t.left.left) >= height(t.left.right)) {
				t = rotateWithLeftChild(t);
			}else {
				t = doubleRotateWithLeftChild(t);
			}
		}else if(height(t.right) - height(t.left) > allowedImbalance) {
			//ensure single rotation
			if(height(t.right.right) >= height(t.right.left)) {
				t = rotateWithRightChild(t);
			}else {
				t = doubleRotateWithRightChild(t);
			}
		}
		t.height = Math.max(height(t.left), height(t.right));		//set new height if tree
		return t;
	}
	
	/*
	 * single rotation with Left Child
	 * Code "maybe" borrowed
	 */
	private AVLNode<E> rotateWithLeftChild(AVLNode<E> k2){
		AVLNode<E> k1 = k2.left;
		k2.left = k1.right;
		k1.right = k2;
		k2.height = Math.max(height(k2.left), height(k2.right));
		k1.height = Math.max(height(k1.left), height(k1.right));
		return k1;
	}
	
	/*
	 * double rotation with left child
	 * Code "maybe" borrowed
	 */
	private AVLNode<E> doubleRotateWithLeftChild(AVLNode<E> k3){
		k3.left = rotateWithRightChild(k3.left);
		return rotateWithLeftChild(k3);
	}
	
	/*
	 * single rotation with right child
	 * Code "maybe" borrowed
	 */
	private AVLNode<E> rotateWithRightChild(AVLNode<E> k1){
		AVLNode<E> k2 = k1.right;
		k1.right = k2.left;
		k2.left = k1;
		k1.height = Math.max(height(k1.left), height(k1.right));
		k2.height = Math.max(height(k2.left), height(k2.right));
		return k2;
	}
	
	/*
	 * Double rotation with Right child
	 * code "maybe" borrowed
	 */
	private AVLNode<E> doubleRotateWithRightChild(AVLNode<E> k3){
		k3.right = rotateWithLeftChild(k3.right);
		return rotateWithRightChild(k3);
	}
	
	//helper method to remove a node from the tree
	private AVLNode<E> remove(E x, AVLNode<E> t){
		if(t == null) {
			//tree empty
			return t;
		}
		
		int compareResult = x.compareTo(t.element);
		
		if(compareResult < 0) {
			t.left = remove(x, t.left);
		}else if(compareResult > 0) {
			t.right = remove(x, t.right);
		}else if(t.left != null && t.right != null) {
			//Two Children
			t.element = t.right.left.element;
			t.right = remove(t.element, t.right);
		}else {
			t = (t.left != null) ? t.left : t.right;
		}
		return balance(t);								//return balanced tree
	}
	
	//Helper method to check if tree contains required element
	private boolean contains(E x, AVLNode<E> t) {
		while(t!=null) {
			int compareResult = x.compareTo(t.element);
			
			if(compareResult < 0) {
				t = t.left;
			}else if(compareResult > 0) {
				t = t.right;
			}else {
				return true; //match found
			}
		}
		
		return false;	//match not found
	}
	
	//makes the tree empty
	public void makeEmpty() {
		this.root = null;
	}
	
	public static final int NUM_TIMINGS = 5;
	
	public static void main(String args[]) {
		
		 for(int timing = 0; timing < NUM_TIMINGS; ++timing) {
	            long startTime = System.nanoTime();

	            // ... The code being timed ...
	            // Replace this code with your own code:
	            // begin code to replace:
	                     
	            // end code to replace

	            long endTime = System.nanoTime();
	            long elapsedTime = endTime - startTime;
	            // 1 second = 1000000000 (10^9) nanoseconds.
	            System.out.println(TimeUnit.NANOSECONDS.toMillis(elapsedTime) + " milliseconds");
	        }
   }

}

