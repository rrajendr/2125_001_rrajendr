import java.util.List;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/*
 * @author: Rishav Rajendra
 * @Class: CSCI 2125
 * This class implements the Perfect Hash Map
 * A lot of help was taken from java docs and github
 */

public class PerfectHashMap<K, V> implements Map<K,V> {

	//Class variables
	private final Object[] keys;
	private final Object[] values;
	
	//Constructor
	private PerfectHashMap(Object[] keys, Object[] values) {
		this.keys = keys;
		this.values = values;
	}
	
	/*
	 * Implemented clear() function
	 * Clears the Hash table
	 */
	@Override
	public void clear() {
		for(int i = 0; i < values.length; i++) {
			keys[i] = null;
			values[i] = null;
		}
	}

	/*
	 * Checks to see if key exists
	 */
	@Override
	public boolean containsKey(Object key) {
		return keys[indexForKey(key)] != null;
	}

	//calls helper method indexForKey(key, keyLength)
	private int indexForKey(Object key) {
		if(key == null) {
			throw new NullPointerException("Keys cannot be null");
		}
		return indexForKey(key, keys.length);
	}
	
	/*
	 * Returns the index if key given and size of the the map
	 */
	private static int indexForKey(Object key, int keyLength) {
		/*
		 * key: The key to find index for
		 * keyLength: the size of the map. Must be a power of 2
		 */
		return key.hashCode() & (keyLength -1);
	}

	/*
	 * Implemented method to see if required value exists
	 */
	@Override
	public boolean containsValue(Object value) {
		for(Object val: values) {
			if(val == null && value == null) {
				return true;
			}else if(val != null && val.equals(value)) {
				return true;
			}
		}
		return false;
	}

	/*
	 * Implemented entrySet method
	 */
	@Override
	public Set<Entry<K, V>> entrySet() {
		
		Set<Entry<K, V>> result = new HashSet<Entry<K,V>>(keys.length);
		
		for(final Object key : keys) {
			Entry<K,V> entry = new Entry<K,V>(){
				@Override
				public K getKey() {
					return (K)key;
				}
				
				@Override
				public V getValue() {
					return (V)PerfectHashMap.this.values[PerfectHashMap.this.indexForKey(key)];
				}
				
				@Override
				public V setValue(V value) {
					int i = PerfectHashMap.this.indexForKey(key);
					
					V existingVal = (V)PerfectHashMap.this.values[i];
					
					PerfectHashMap.this.values[i] = value;
					
					return existingVal;
				}
			};
			
			result.add(entry);
		}
		
		return result;
	}

	/*
	 * Implemented get(key) method
	 * Returns value at given key
	 */
	@Override
	public V get(Object key) {
		return (V)values[indexForKey(key)];
	}

	/*
	 * Implemented isEmpty() method
	 * returns boolean 
	 */
	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	/*
	 * Implemented keySet() method
	 * returns keySet
	 */
	@Override
	public Set<K> keySet() {
		Set<K> keySet = new HashSet<K>();
		
		for(Object key: keys) {
			if(key != null) {
				keySet.add((K)key);
			}
		}
		
		return keySet;
	}

	/*
	 * Implemented put() method
	 * returns existing value
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public V put(K key, V value) {
		System.out.println("key: " + key);
		
		int index = indexForKey(key);
		
		System.out.println("index: " + index);
		
		Object existingKey = keys[index];
		Object existingValue = values[index];
		
		System.out.println(existingKey);
		
		if(existingKey != null && !existingKey.equals(key)) {
			throw new IllegalArgumentException("Cannot insert key " + key + " as it is a new key");
		}
		
		keys[index] = key;
		values[index] = value;
		
		return (V)existingValue;
	} 

	/*
	 * Implemented putAll() method
	 * @see java.util.Map#putAll(java.util.Map)
	 */
	@Override
	public void putAll(Map<? extends K, ? extends V> map) {
		System.out.println("Put all");
		for(Entry<? extends K, ? extends V> entry: map.entrySet()) {
			put(entry.getKey(), entry.getValue());
		}
	}

	/*
	 * implemented remove() method
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	@Override
	public V remove(Object key) {
		int index = indexForKey(key);
		
		Object object = values[index];
		
		keys[index] = null;
		values[index] = null;
		
		return (object == null) ? null : (V)object;
	}

	/*
	 * Implement size() method
	 * returns the size of the hash map
	 */
	@Override
	public int size() {
		int size = 0;
		for(Object object: keys) {
			if (object != null) {
				size++;
			}
		}
		return size;
	}

	/*
	 * Implemented values() method
	 * @see java.util.Map#values()
	 */
	@Override
	public Collection<V> values() {
		
		List<V> values = new ArrayList<V>();
		
		for(Object key: keys) {
			if(key != null)values.add(get(key));
		}
		
		return values;
	}
	
	public static <K,V> Builder<K, V> newBuilder(){
		return new Builder<K, V>();
	}
	
	//Class Builder
	public static class Builder<K, V>{
		 private final Map<K, V> map = new HashMap<K, V>();
		 
		 //Constructor
		 private Builder() {
			 //do nothing
		 }
		 
		 //Add function
		 public Builder<K, V> add(K key, V value){
			 
			 map.put(key, value);
			 
			 return this;
		 }
		 
		 //Add all function
		 public Builder<K, V> addAll(Map<K, V> map){
			 for(Entry<K, V> entry: map.entrySet()) {
				 add(entry.getKey(), entry.getValue());
			 }
			 
			 return this;
		 }
		 
		 public <K_1 extends K, V_1 extends V> Map<K_1, V_1> build(){
			 if(map.size() == 0) {
				 return new PerfectHashMap<K_1, V_1>(new Object[0], new Object[0]);
			 }
			 
			 assertCollisionFree();
			 
			 int size = findSize();
			 
			 Object[] keys = new Object[size];
			 Object[] values = new Object[size];
			 
			 for(Entry<K, V> entry: map.entrySet()) {
				 int index = indexForKey(entry.getKey(), size);
				 
				 keys[index] = entry.getKey();
				 values[index] = entry.getValue();
			 }
			 
			 return new PerfectHashMap<K_1, V_1>(keys, values);
		 }
		 
		 /*
		  * Find size of table
		  */
		 public int findSize() {
			 Set<K> keys = map.keySet();
			 
			 //Find a power of 2 capacity > map.size();
			 int size = 1;
			 
			 while(hasCollisions(size, keys)) {
				 size <<=1;
			 }
			 
			 return size;
		 }

		 //Check fill rate
		 public float fillrate() {
			 if(map.size() == 0) {
				 return 0;
			 }
			 
			 int size = findSize();
			 
			 return (float)map.size()/size;
		 }
		 
		 /*
		  * Check if if has collisions
		  */
		private boolean hasCollisions(int size, Set<K> keys) {
			
			final BitSet usedIndexes = new BitSet(size);
			
			for(K key: keys) {
				int index = indexForKey(key, size);
				
				if(usedIndexes.get(index)) {
					return true;
				}
				
				usedIndexes.set(index);
			}
			
			return false;
		}

		//Make collision free
		private void assertCollisionFree() {
			final Set<Integer> hashCodes = new HashSet<Integer>();
			
			for(K key: map.keySet()) {
				int hashCode = key.hashCode();
				
				if(hashCodes.contains(hashCode)) {
					throw new IllegalArgumentException("HashCode bang bang aka collision");
				}
				
				hashCodes.add(hashCode);
				
			}
			
		}
	}
	
	public static final int NUM_TIMINGS = 5;
	
	public static void main(String args[]) {
		
		int num = 1000000;
		
        for(int timing = 0; timing < NUM_TIMINGS; ++timing) {
        	
            long startTime = System.nanoTime();

            // ... The code being timed ...
            // Replace this code with your own code:
            // begin code to replace:
            
            
            // end code to replace

            long endTime = System.nanoTime();
            long elapsedTime = endTime - startTime;
            // 1 second = 1000000000 (10^9) nanoseconds.
            System.out.println(TimeUnit.NANOSECONDS.toMillis(elapsedTime) + " milliseconds");
            
            
        }
        
	}

}
