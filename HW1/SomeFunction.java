public class SomeFunction implements Function{
		
	/**
	 * Function used y=2x^2-4x-75
	 */
	
	public double f(double x){
		return 2*x*x-4*x-75;
	}
}