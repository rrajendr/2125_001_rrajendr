/**
*This is a method that uses the idea of Binary Search 
*to search for the zero of a function
*leftOfInterval: 		The point on the left of the current interval
*midPointOfInterval: 	The mid-point of the current interval
*rightOfInterval: 		The point on the right of the current interval
*fPoint1: 				the value of the function at point leftOfInterval
*fPoint2: 				the value of the function at point midPointOfInterval
*fPoint3: 				the value of the function at point rightOfInterval
*root:					the zero of the function
*/

public class NumericalAnalysis{
	public static double solveForZero(Function function, double low, double high){
		
		//points on the left, midpoint and the right of the current interval
		double leftOfInterval;
		double midpointOfInterval;
		double rightOfInterval;

		//value of function calculated at these points
		double fPoint1;
		double fPoint2;
		double fPoint3;
		
		//To initialise the variable
		double root = 0.0;
		
		/**
		*Calculate the root
		*/
		leftOfInterval = low;
		rightOfInterval = high;
		
		fPoint1 = roundNumber(function.f(leftOfInterval), 5);
		fPoint3 = roundNumber(function.f(rightOfInterval), 5);
		
		/**
		*root existes in interval if f(low) and f(high) and have opposite signs
		*/
		if(fPoint1 * fPoint3 > 0){
		}else{
			//find the mid-point of the interval for binary sort
			midpointOfInterval = (leftOfInterval + rightOfInterval)/2;

			fPoint2 = roundNumber(function.f(midpointOfInterval),5);
			
			/**
			*Root is in the left half of the interval if fPoint1 and fPoint2 <=0
			*/
			if(fPoint1 * fPoint2 <= 0){
				solveForZero(function, low, roundNumber(midpointOfInterval, 5));
			}else{
				//root is in the right half of the interval
				solveForZero(function, roundNumber(midpointOfInterval,5), high);
			}
			if(fPoint2 < 0.00001) {
				root = midpointOfInterval;
			}
		}
		return root;
	}
	
	//function to round the double to 5 decimal places
	public static double roundNumber(double number, int roundTo) {
		long factor = (long)Math.pow(10, roundTo);
		number = number * factor;
		long temporary = Math.round(number);
		return (double)temporary/factor;
	}
}